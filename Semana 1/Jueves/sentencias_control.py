# Sentencias condicionales
# if else elseif
edad = 17

# if edad < 18:
#     print("Soy menor de edad")
# elif edad == 18:
#     print("Tengo 18 años")
# else:
#     print("Mayor de edad")

# Bucles
# while

# Control de bucles
# break
# continue
# pass
# x = 0
# while True: # True
#     if x == 5:
#         break
#     print(x)
#     x += 1

# for
# nueva_lista = [1, 2, 3, 4]
for i in range(10): # 0
    if i % 2 == 0:
        pass
        # continue # En el algun momento colocaremos una logica
    else:
        print(i)

def funcion_nueva():
    pass
