# Funcion (def)

# Funcion sin parametros
def saludar():
    print('Hola !')

# Funcion con parametros
def saludar(nombre):
    print(f'Hola, {nombre} !')

# Función con retorno de resultado
def sumar(n_uno, n_dos):
    """
    Devuelve la suma de dos valores
    args:
        - n_uno: Primer valor
        - n_dos: Segundo valor
    return:
        Suma de n_uno y n_dos
    """
    return n_uno + n_dos

# Función con parametros valores por defecto
def saludar(edad=29, nombre="Stephan"):
    # edad si esta vacio = 29
    print(f'Hola {nombre}, tu edad es {edad} años')

## Funciones con multipleparametros
# Argumentos variables posicionales (*)
# def sumar(*args): # sumar(1, 2, 3, 4, 5)
#     return sum(args)

# Argumentos variable de palabras clave (**)
def mostrar_datos(nombre, edad, ciudad): # mostrar_datos(nombre="Jeancarlos", edad=29)
    print(nombre, edad, ciudad) # print(kwargs)

# datos = {
#     'nombre': 'Jeancarlos',
#     'edad': 29,
#     'ciudad': 'Lima'
# }
# 
# mostrar_datos(**datos)

# Funcion lambda
# lambda argumentos: expresion
# suma = lambda x, y: x + y
# print(suma(5, 3))

# TODO: -
help(sumar)
