# Escribir un codigo donde pida ingresar una edad y que cumpla las siguientes condiciones
# - Si es menor a 18, me debes mostrar un mensaje que no puedo ingresar al concierto
# - Si es mayor a 18, me debe mostrar que ingrese al concierto

# Escribir un codigo donde pida ingresar un valor que sea mayor a 80, y debe cumplir las siguientes 
# condiciones :
# - Si no es mayor a 80 el valor ingresado, me debe mostrar un mensaje que indique que el valor sea > a 80.
####################################################################################
# - Si el valor es mayor a 85, me debe mostrar un mensaje que diga Excelente
# - Si el valor es menor igual a 80, me debe mostrar un mensaje que diga Bien
# - Si el valor es igual a 100, me debe mostrar un mensaje que diga Extraordinario

# def evaluar_valor(valor):
#     if valor < 80:
#         return 'El valor debe ser mayor a 80'
#     elif valor > 85:
#         if valor == 100:
#             return 'Extraordinario'
#         else:
#             return 'Excelente'
#     else:
#         return 'Bien'
#     
# valor = int(input("Ingresa un valor mayor a 80: "))
# mensaje = evaluar_valor(valor)
# print(mensaje)

# Tengo el siguiente diccionario :
diccionario = {
    'a': 10,
    'b': 20,
    'c': 30
}
# Se debe sumar los elementos del diccionario
def sumar_valores(diccionario):
    return sum(diccionario.values())

# print(sumar_valores(diccionario))

# Tenemos la siguiente lista
lista = [1, 8, 35, 14, 9, 25, 11]
# Se debe obtener el valor mayor de la lista
# print(max(lista))


# Reglas de collatz
# Todos los numeros deben ser entero positivo
# Si n es par, dividirlo por 2 para obtener 'n / 2'
# Si n es impar, multplicarlo por 3 y sumarle 1 para obtener '3n + 1'
# Repetir las veces que sean necesarias con el valor de n
# Cuando obtenga 1, la serie termina

# Numero: 19 
# 19 58 29 88 ...... 1

# Crear una función que genere una lista con la secuencia de fibonacci hasta un numero limite (10)
# Secuencia de numeros naturales, apartir de 0 y el 1
# Se van a sumar los numeros pares, de tal forma que cada numero es igual a la suma de los dos anteriores
# 0 1 1 2 3 5 8 13 21...... 

def fibonacci(limite):

    if limite == 1:
        return [0, 1]

    a, b = 0, 1

    secuencia = [a]

    while b <= limite:
        secuencia.append(b)
        a, b = b, a + b
    
    return secuencia

print(fibonacci(20))


