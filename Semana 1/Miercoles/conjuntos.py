# Desordenada, no tiene indices y no permite valores duplicados
nuevo_conjunto = {1, 2, 3, 4, 5}

print(f'original -> {nuevo_conjunto}')

# add -> Agregar un elemento al conjunto
nuevo_conjunto.add(6)
print(f'add -> {nuevo_conjunto}')

# remove -> Eliminar un elemento al conjunto
nuevo_conjunto.remove(6)
print(f'remove -> {nuevo_conjunto}')

# discard -> Eliminar un elemento si este existe
nuevo_conjunto.discard(8)
print(f'discard -> {nuevo_conjunto}')

print("################################################")

conjunto_uno = {1, 2, 3}
conjunto_dos = {3, 4, 5}

# Union
union = conjunto_uno.union(conjunto_dos)
print(f'union -> {union}')

# Intersección
interseccion = conjunto_uno.intersection(conjunto_dos)
print(f'interseccion -> {interseccion}')

# Diferencia
diferencia = conjunto_uno.difference(conjunto_dos)
print(f'diferencia -> {diferencia}')

# Diferencia simetrica
diferencia_simetrica = conjunto_uno.symmetric_difference(conjunto_dos)
print(f'diferencia simetrica -> {diferencia_simetrica}')
