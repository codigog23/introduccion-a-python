# Ordenada, inmutable y permiten valores duplicados (tipo dinamico)
# posicion -> indice
# n - 1
nueva_tupla = (1, 2, 3, 4, 5)

print(f'original -> {nueva_tupla}')

# Acceder a un elemento de la tupla
elemento = nueva_tupla[2]
print(f'acceder -> {elemento}')

## count -> Cuenta las veces en la que se repite un valor
contar_dos = nueva_tupla.count(2)
print(f'count -> {contar_dos}')

## index -> Retornar el indice del valor definido
indice = nueva_tupla.index(4)
print(f'index -> {indice}')

# NO SE PUEDE CREAR UNA TUPLA CON UN ELEMENTO SIN UNA COMA
tupla_simple = (1,)
print(type(nueva_tupla), type(tupla_simple))

###########################################################

# Añadir un elemento a una tupla
tupla_lista = list(nueva_tupla)
tupla_lista.append(6)

lista_tupla = tuple(tupla_lista)
print(lista_tupla)
