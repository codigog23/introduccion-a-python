# Funcion (def)

# Funcion sin parametros
def saludar():
    print('Hola !')

# Funcion con parametros
def saludar(nombre):
    print(f'Hola, {nombre} !')

# Función con retorno de resultado
def sumar(n_uno, n_dos):
    return n_uno + n_dos

print(sumar(10, 5))
