# Javascript -> Array []
# Ordenada, mutable y permiten valores duplicados (tipo dinamico)
# posicion -> indice
# n - 1
nueva_lista = [1, 2, 3, 4, 5] # nueva_lista[2]
nueva_lista_dos = [1, "a", "$", [6, 7]] # nueva_lista_dos[1]

print(f'original -> {nueva_lista}')

# Metodos
## append -> Agrega un elemento al final de una lista
nueva_lista.append(6)
print(f'append -> {nueva_lista}')

## insert -> Inserta un elemento en la posición definida
nueva_lista.insert(0, 10)
print(f'insert -> {nueva_lista}')

## pop -> Elimina un elemento segun la posicion definida
nueva_lista.pop(0)
print(f'pop -> {nueva_lista}')

nueva_lista.pop() # el ultimo valor (indice)
print(f'pop sin indice -> {nueva_lista}')

## count -> Cuenta las veces en la que se repite un valor
contar_dos = nueva_lista.count(2)
print(f'count -> {contar_dos}')

## extend -> Extender una lista con otra lista
nueva_lista.extend([6, 7, 8])
print(f'extend -> {nueva_lista}')

## index -> Retornar el indice del valor definido
indice = nueva_lista.index(3)
print(f'index -> {indice}')

## Ordenar
# Mayor a menor (reverse)
nueva_lista.reverse()
print(f'reverse -> {nueva_lista}')

# Menor a mayor (sort)
nueva_lista.sort()
print(f'sort -> {nueva_lista}')

# Mayor a menor (sort)
nueva_lista.sort(reverse=True)
print(f'sort con reverse -> {nueva_lista}')
