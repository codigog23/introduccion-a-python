![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# Estructuras de datos

## Mutables

## Inmutables

```js
/* Spread Operator */
const array_uno = [1, 2, 3];
const array_dos = [4, 5, 6];

const array_union = [...array_uno, ...aray_dos];

/* destructuring */
const json_persona = {
  nombre: "Jeancarlos",
  edad: 29,
};

const { nombre, edad } = json_persona;
```
