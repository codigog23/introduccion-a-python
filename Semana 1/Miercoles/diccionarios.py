# No ordenado, mutables, no permite duplicados, se base en clave y valor (key => value)
nuevo_diccionario = {
    "nombre": "Jeancarlos",
    "edad": 29
}

print(f'original -> {nuevo_diccionario}')

# Acceder a un valor atraves de la clave
# nombre = nuevo_diccionario["nombre"]
# print(f'clave nombre -> {nombre}')

# Agregar un nuevo valor al diccionario
nuevo_diccionario["ciudad"] = "Lima"
print(f'nuevo -> {nuevo_diccionario}')

# Eliminar un valor mediante la clave
del nuevo_diccionario["edad"]
print(f'del -> {nuevo_diccionario}')

# Validar si una clave existe
existe = "nombre" in nuevo_diccionario
print(f'in -> {existe}')

# Obtener las claves de un diccionario
claves = nuevo_diccionario.keys()
print(f'keys -> {claves}')

# Obtener todos los valores de un diccionario
valores = nuevo_diccionario.values()
print(f'values -> {valores}')

# get -> obtiene el valor desde una clave, si la clave no existe retornara un nulo
pais = nuevo_diccionario.get("pais", "Perú") # el argumento es el valor por defecto si no existiera la clave
print(f'get -> {pais}')

# combinar dos diccionarios
otro_diccionario = { "casado": True, "hijos": 0 }

# 1) Primera forma
# nuevo_diccionario.update(otro_diccionario)
# print(f'update -> {nuevo_diccionario}')

# 2) Segunda forma (Desempaquetado)
# diccionario_combinado = {**nuevo_diccionario, **otro_diccionario}
# print(f'desempaquetado -> {diccionario_combinado}')

# 3) Tercera forma (El uso del operador union |)
diccionario_combinado = nuevo_diccionario | otro_diccionario
print(f'operador union -> {diccionario_combinado}')

########################################################

# Desempaquetado Basico
# nombre, ciudad = diccionario_combinado["nombre"], diccionario_combinado["ciudad"]
# print(nombre, ciudad)
