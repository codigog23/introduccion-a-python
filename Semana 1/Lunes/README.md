![backend](https://media.licdn.com/dms/image/D4D12AQEk0NzSbdzHoQ/article-cover_image-shrink_600_2000/0/1695824031690?e=2147483647&v=beta&t=i_NaJ2sxBWs0IofeXAi9AnPUxXzOns0J05iGWEXp0NE)

# 1. Que es un backend

![back-database](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQHBxjDMCUU8OTAPbiGRCMPmLam0L_uDArmIEzTW5Q60vbMxgqgNwR0vtwWzIAN5cx3ow&usqp=CAU)

# 2. Lenguajes y Frameworks

![dev](https://blog.back4app.com/wp-content/uploads/2020/09/backend-solutions-3-1140x515.png)

# 3. Roadmap Backend

[BackendDeveloper](https://roadmap.sh/backend)

# 4. Pseudocodigo

![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# 1. Historia

# 2. ¿Porqué usarlo?

# 3. Semántica y Sintaxis

- Sintaxis

```js
if (condicion) {
  /* bloque de codigo */
}
```

```py
if condicion:
    # bloque de codigo
```

- Semántica

```js
let array = [];
array.push(1);
/////////////////////
console.log(array);
```

```py
array = []
array.append(1)
#####################
print(array)
```
