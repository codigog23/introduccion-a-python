![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

## Pseudocodigo

```sh
# Se cree un programa que sume todos los números del 1 al 10

Inicializamos suma a 0
Para cada número n del 1 al 10
    Sumar n a suma
Fin del bucle
Mostrar suma

# Quiero tomar un café caliente, con azucar normal
# - Validar si ya hay cafe hecho
1. ¿Hay café hecho?
2. Servir en una taza y añadir azucar
3. ¿Esta dulce?
4. Tomar el café

# Freir un huevo de gallina, en la cocina usando la sarten, los huevos a usar se encuentran en la refrigeradora.
# - Validar si hay aceite
# - Validar si el huevo esta frito
# - Validar si tiene sal
1. ¿Hay aceite?
2. Prender la cocina y colocar el sarten
3. Echar el huevo
4. ¿Esta frito?
5. ¿Tiene sal?
6. Servir en el plato y comerlo

```

![diagrama](diagrama_flujo.drawio.svg)

## Operadores de entrada y salida

## Definición de variables

## Tipos de datos

## Operadores

#### Ejemplo:

```js
let a = 5;
let b = "5";

console.log(a == b); // true
console.log(a === b); // false
```
