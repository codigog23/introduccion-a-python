# Operadores (control) de entrada y salida

# Entrada
# input()
# nombre = input("Ingresa tu nombre: ")

# JS -> parseInt()
# PY -> int()
# edad = int(input("Ingresa tu edad: "))

# Salida
# print()

#print("Hola mundo !")
#print("Hola, " + nombre)

# Formato de cadenas de texto
# 1) Usando el operador %
# print("Hola, %s tienes %d años" % (nombre, edad))

# 2) Usando la funcion format (< 3.6)
# print("Hola, {} tienes {} años".format(nombre, edad))

# JS -> `Hola, ${nombre} tienes ${edad} años`
# 3) Usando f-string (>= 3.6)
# print(f"Hola, {nombre} tienes {edad} años")
