## Operadores Aritmeticos
# Suma (+)
# a = 5 + 3
# b = 4.5 + 2.5
# c = 5 + (-3)

# Resta (-)
# a = 5 - 3
# b = 4.5 - 2.5
# c = 5 - 8

# Multiplicación (*)
# a = 5 * 3
# b = 4.5 * 2
# c = 5 * (-3)

# División Regular (flotante)
# a = 5 / 2
# b = 9 / 2
# c = 4 / 2

# División Exacta (enteros)
# a = 10 // 2
# b = 8 // 2
# c = 9 // 2

# Resto o Modulo (%) - Devuelve el resto de una división
# Tip: Si desean saber si un número es par o impar
# a = 4 % 2
# b = 3 % 2
# c = 8 % 3

#print(a, b, c)

## Operadores de asignación
# Asignación (=)
# a = 5

# Suma y asignación
# a += 3

# Resta y asignación
# a -= 2

## Operadores de comparación
# Igual a (==)
# print(5 == 5)
# print(5 == 8)

# Diferente de (!=)
# print(5 != 5)
# print(5 != 7)

# Menor a (<)
# print(3 < 2)

# Menor igual a (<=)
# print(4 <= 4)

# Mayor a (>)
# print(10 > 5)

# Mayor igual a (>=)
# print(5 >= 5)

# Operador de identidad (is)
a = [1, 2, 3]
b = [1, 2, 3]

print(a == b)
print(a is b)
