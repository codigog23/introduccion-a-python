# Tip: Duck typing
# 1) El nombre no puede empezar con un número
# 2) No se puede usar guiones (-)
# 3) No se puede usar espacios

# Tip: Variable constante, se debe usar el nombre 
# de la variables en mayuscula (visual)
IGV = 0.18 # constante
mi_variable = 5
mi_variable = "Hola"

# Evitar las palabras reservadas
'''
['False', 'None', 'True', 'and', 'as', 'assert', 'async', 'await', 'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 
'raise', 'return', 'try', 'while', 'with', 'yield']
'''