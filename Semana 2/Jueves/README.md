![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# Diseña una BD

Debemos crear un sistema, donde vamos a guardar las ventas relacionas con diferentes productos (libros).

1. Definir el nombre de la BD (biblioteca)
2. Definir las tablas
   - ventas
     - id_venta
     - nombre_cliente
     - libro_comprado
     - autor
     - precio
     - direccion_cliente

| id_venta | nombre_cliente   | libro_comprado | autor   | precio | direccion_cliente     |
| -------- | ---------------- | -------------- | ------- | ------ | --------------------- |
| 1        | Josue Jauregui   | Libro A        | Autor X | 100.00 | Calle 123, Ciudad X   |
| 2        | Josue Jauregui   | Libro B        | Autor Y | 150.00 | Calle 123, Ciudad X   |
| 3        | Stephan Olivares | Libro A        | Autor X | 100.00 | Avenida 789, Ciudad Z |

1. La direccion de Josue, la combinacion del Libro A con Autor X, se repiten (Redundancia).
2. Si la dirección de Josue cambia, deberemos actualizar tambien todos los registros donde aparezca Josue.
3. Si un libro cambia de precio, tambien debemos actualizar en todos los registros donde aparezca el mismo.

# Normalización

1. Eliminar la redundancia de datos (los mismos datos almacenados en varios lugares).
2. Asegurar la dependencia logica.
3. Facilitar la inserción, modificación y la eliminación de la data.

## Primera Forma Normalización (1NF)

Cada columna de una tabla tiene un valor unico y cada registro es identificado de manera unica.

## Segundo Forma Normalización (2NF)

Cada tabla debe contar por lo menos con un campo que tenga una llave primaria o llave foranea.

## Tercera Forma Normalización (3NF)

Cada tabla se debe de evaluar si va a tener clave primaria o clave foranea

### Normalización de tabla ventas

**Tabla: Clientes**

| id  | nombres          | direccion             |
| --- | ---------------- | --------------------- |
| 1   | Josue Jauregui   | Calle 123, Ciudad X   |
| 2   | Stephan Olivares | Avenida 789, Ciudad Z |

**Tabla: Libros**

| id  | titulo  | autor   | precio |
| --- | ------- | ------- | ------ |
| 1   | Libro A | Autor X | 100.00 |
| 2   | Libro B | Autor Y | 150.00 |

**Tabla: Ventas**

| id  | id_cliente | id_libro |
| --- | ---------- | -------- |
| 1   | 1          | 1        |
| 2   | 1          | 2        |
| 3   | 2          | 1        |

#### Ejercicio 1

- Normalizar la siguiente tabla:

**Tabla: Estudiantes**

| id  | nombre | apellido | edad | direccion | telefono | curso | profesor |
| --- | ------ | -------- | ---- | --------- | -------- | ----- | -------- |
|     |        |          |      |           |          |       |          |

| id  | nombre_alumno | Apellido_alumno | edad | direccion              | telefono |
| --- | ------------- | --------------- | ---- | ---------------------- | -------- |
| 1   | Juan          | Snow            | 24   | Av los ciruelos 123    | 1234     |
| 2   | Miguel        | Man             | 23   | Av los costructore 123 | 4567     |
| 3   | Luiz          | Barbosa         | 24   | Av Atom 123            | 457      |

**Tabla: Curso**

| id  | profesor | curso           |
| --- | -------- | --------------- |
| 1   | Leitol   | Algebrra Lineal |
| 2   | MacGill  | Calculo         |

**Tabla: Inscripciones**

| id  | id_alumno | id_curso |
| --- | --------- | -------- |
| 1   | 1         | 1        |
| 2   | 2         | 1        |
| 3   | 3         | 2        |

#### Ejercicio 2

- Normalizar la siguiente tabla:

**Tabla: PrestamosBiblioteca**

| id  | fecha_prestamo | nombre_lector | titulo_libro         | autor_libro         |
| --- | -------------- | ------------- | -------------------- | ------------------- |
| 1   | 2023-12-05     | Ana Ruiz      | Cien años de soledad | G. Garcia M.        |
| 2   | 2023-11-04     | Luis Perez    | Don Quijote          | M. de Cervantes     |
| 3   | 2023-12-01     | Ana Ruiz      | El principito        | A. De Saint-Exupéry |
| 4   | 2022-02-12     | Maria Lopez   | Cien años de soledad | G. Garcia M.        |

**Tabla: Lectores**

| id  | nombres     |
| --- | ----------- |
| 1   | Ana Ruiz    |
| 2   | Luis Perez  |
| 3   | Maria Lopez |

**Tabla: Libros**

| id  | titulo               | autor               |
| --- | -------------------- | ------------------- |
| 1   | Cien años de soledad | G. Garcia M.        |
| 2   | Don Quijote          | M. de Cervantes     |
| 3   | El principito        | A. De Saint-Exupéry |

**Tabla: Prestamos**

| id  | fecha      | id_lector | id_libro |
| --- | ---------- | --------- | -------- |
| 1   | 2023-12-05 | 1         | 1        |
| 2   | 2023-11-04 | 2         | 2        |
| 3   | 2023-12-01 | 1         | 3        |
| 4   | 2022-02-12 | 3         | 1        |

```sql
SELECT * FROM prestamos
INNER JOIN lectores ON prestamos.id_lector = lectores.id
INNER JOIN libros ON prestamos.id_libro = libros.id
INNER JOIN autores ON libros.id_autor = autores.id
```

# Tipos de lenguajes para gestionar una BD

## Comandos en SQL Shell (psql)

- Listar las base de datos: **\l** o **\list**
- Seleccionar una base de datos: **\c nombre_bd**
- Listar las tablas de mi BD (despues de seleccionar una BD): **\dt**
- Listar los campos de una tabla: **\d nombre_tabla**

## 1. DDL (Data Definition Language)

- CREATE: Utilizar para crear objetos como BD, tablas, vistas, indices, etc.

```sql
-- Crear una BD
CREATE DATABASE nombre_bd;

-- Crear una tabla
CREATE TABLE nombre_tabla (
  -- Definimos los campos
  -- nombre_campo tipo_dato opciones
  -- https://www.ibiblio.org/pub/Linux/docs/LuCaS/Tutoriales/NOTAS-CURSO-BBDD/notas-curso-BD/node134.html
);

-- Ejemplo TABLA Clientes
CREATE TABLE clientes (
  id SERIAL PRIMARY KEY.
  nombre VARCHAR(50),
  correo VARCHAR(100)
);
```

- ALTER: Modificar cualquier objeto de la BD

```sql
ALTER TABLE nombre_tabla -- comandos
-- ADD -> AGREGAR
-- ALTER -> MODIFICAR
-- RENAME -> RENOMBRAR
-- DROP -> ELIMINAR

-- Ejemplo
-- Agregar una columna nueva
ALTER TABLE nombre_tabla ADD COLUMN nombre_campo tipo_dato;

-- Modificar una columna ya existente
ALTER TABLE nombre_tabla ALTER COLUMN nombre_campo tipo_dato;

-- Renombrar una columna ya existente
ALTER TABLE nombre_tabla RENAME COLUMN nombre_campo TO nombre_nuevo_campo;

-- Renombrar una tabla
ALTER TABLE nombre_tabla RENAME TO nombre_nuevo_tabla;

-- Eliminar un campo existente
ALTER TABLE nombre_tabla DROP COLUMN nombre_campo;
```

- DROP: Eliminar cualquier objeto de la BD.

```sql
-- Eliminar una BD
DROP DATABASE nombre_bd;

-- Eliminar una tabla
DROP TABLE nombre_tabla;
```

## 2. DML (Data Manipulation Language)

- INSERT: Inserta nuevos datos en una tabla

```sql
INSERT INTO nombre_tabla (campos) VALUES (valores);
```

- SELECT: Recuperar datos de una tabla

```sql
SELECT * FROM nombre_tabla; -- Traer todos los registros con todas sus columnas

SELECT campo_1, campo_2 FROM nombre_tabla; -- Traer todos los registros solo con las columnas mencionadas
```

- UPDATE: Modificar datos existentes en una tabla

```sql
UPDATE nombre_tabla SET campo = nuevo_valor WHERE campo = valor;
```

- DELETE: Elimina registros existentes en una tabla;

```sql
DELETE FROM nombre_tabla WHERE campo = valor;
```

## Ejercicios

### Ejercicio 1

1. Crear una base de datos: **minimarket**
2. Crear una tabla llamada: **productos**
   - Los campos seran:
     - id (SERIAL PK)
     - nombre (VARCHAR)
     - precio (FLOAT)
3. Ejecutar los 3 comandos DML (SELECT, INSERT, UPDATE)

```sql
-- CREAR TABLA PRODUCTOS
CREATE TABLE productos (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(120),
	precio NUMERIC(5, 2) -- 99999.99
);

-- INSERT
INSERT INTO productos (nombre, precio) VALUES ('Jabon', 1.20);

-- SELECT
SELECT * FROM productos;
SELECT nombre, precio FROM productos;

-- UPDATE
UPDATE productos SET precio = 0.90 WHERE id = 1;
```

## CLAUSULAS JOIN

1. INNER JOIN: Devuelve todas las filas que hay en dos tablas (o mas), siempre y cuando haya una relación.

2. LEFT JOIN: Devuelve todas la filas de la tabla izquierda y solo las coincidencias de la tabla derecha.

3. RIGHT JOIN: Devuelve todas las filas de la tabla derecha y solo las coincidencias de la tabla izquierda.

4. FULL JOIN: Devuelve todas las filas tengan una coincidencia o no.

```sql
-- CREACION DE TABLAS
CREATE TABLE areas (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(60)
);

CREATE TABLE empleados (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(120),
	area_id INT
);

-- INSERTAR DATOS
INSERT INTO areas (nombre) VALUES
('RRHH'),
('Marketing'),
('TI'),
 ('Contabilidad');

INSERT INTO empleados (nombre, area_id) VALUES
('Ana Maria', 1),
('Luis Carlos', 3),
('Maria Mercedes', 2),
('Jose Luis', NULL);

-- RECUPERAR DATOS
SELECT * FROM areas;
SELECT * FROM empleados;

-- JOINS
-- INNER JOIN
SELECT e.id, e.nombre, a.nombre as area FROM empleados e INNER JOIN areas a ON e.area_id = a.id;

-- LEFT JOIN
SELECT e.*, a.* FROM empleados e LEFT JOIN areas a ON e.area_id = a.id;

-- RIGHT JOIN
SELECT e.*, a.* FROM empleados e RIGHT JOIN areas a ON e.area_id = a.id;

-- FULL JOIN
SELECT e.*, a.* FROM empleados e FULL JOIN areas a ON e.area_id = a.id;
```

---

### FULL JOIN

- 2 TABLAS
  1. inventario_2022: Contiene todos los productos y la cantidad del año 2022.
  2. inventario_2023: Contiene todos los productos y la cantidad del año 2023.

1. Obtener todos los productos que estaban en stock en el 2022 pero no en el 2023.
2. Obtener todos los productos que estaban en stock en el 2023 pero no en el 2022.
3. Obtener todos los productos que estaban en stock en ambos años.

```sql
SELECT
COALESCE(iv_d.producto, iv_t.producto) as producto,
iv_d.cantidad as cantidad_2022,
iv_t.cantidad as cantidad_2023,
FROM inventario_2022 iv_d
FULL JOIN invetario_2023 iv_t ON iv_d.producto = iv_t.producto;
```
