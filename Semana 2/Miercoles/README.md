![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# Ejercicios

# Base de datos

- Conjunto estructurado, donde vamos a almacenar información, de la cual iremos gestionandolo mas adelante.

1. Almacenamiento estructurado
2. Acceso eficiente
3. Consistencia de datos
4. Integridad de datos
5. Seguridad
6. Facilidad de gestion

# Tipos de Base de datos

1. Relacionales (MySQL, PostgreSQL, Oracle, SQLServer, etc).
2. No relaciones - NoSQL (MongoDB, Cassandra, Neo4j, Redis, etc).
3. Basadas en objetos (db4o).
4. Basadas en documentales.

## Componentes claves

- Datos: La información que almacenamos.
- Tablas: Es donde los datos guardados se organizaran.
- Motor de BD (DBMS): Es el software que se usara para interactuar con la BD.
- Índices: Estructuras que mejoran la velocidad de consultas realizadas a la BD.
- Claves (KEY): Campos especificos que ayudan a identificar un registro unico.

# Diseña una BD

Debemos crear un sistema, donde vamos a guardar las ventas relacionas con diferentes productos (libros).

1. Definir el nombre de la BD (biblioteca)
2. Definir las tablas
   - ventas
     - id_venta
     - nombre_cliente
     - libro_comprado
     - autor
     - precio
     - direccion_cliente

| id_venta | nombre_cliente   | libro_comprado | autor   | precio | direccion_cliente     |
| -------- | ---------------- | -------------- | ------- | ------ | --------------------- |
| 1        | Josue Jauregui   | Libro A        | Autor X | 100.00 | Calle 123, Ciudad X   |
| 2        | Josue Jauregui   | Libro B        | Autor Y | 150.00 | Calle 123, Ciudad X   |
| 3        | Stephan Olivares | Libro A        | Autor X | 100.00 | Avenida 789, Ciudad Z |

1. La direccion de Josue, la combinacion del Libro A con Autor X, se repiten (Redundancia).
2. Si la dirección de Josue cambia, deberemos actualizar tambien todos los registros donde aparezca Josue.
3. Si un libro cambia de precio, tambien debemos actualizar en todos los registros donde aparezca el mismo.

# Normalización

1. Eliminar la redundancia de datos (los mismos datos almacenados en varios lugares).
2. Asegurar la dependencia logica.
3. Facilitar la inserción, modificación y la eliminación de la data.

# Tipos de lenguajes para gestionar una BD

1. DML
2. DDL
