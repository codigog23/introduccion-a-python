# Ejercicio 1 
# Crear una calculadora simple, debe realizar las operaciones basicas (suma, resta, multiplicacion y division)
# utilizar una clase y metodos

class Calculadora:
    def suma(self, a, b):
        return a + b
    
    def resta(self, a, b):
        return a - b
    
    def multiplicacion(self, a, b):
        return a * b
    
    def division(self, a, b):
        try:
            return a / b
        except ZeroDivisionError:
            return 'No se puede dividir por cero'
        except TypeError:
            return 'Solo aceptan numero enteros'
        except Exception as e:
            print(e.__class__)
            return e

# calculadora = Calculadora()
# print(calculadora.suma(5, 4))
# print(calculadora.resta(6, 2))
# print(calculadora.multiplicacion(5, 2))
# print(calculadora.division(10, 5))
# print(calculadora.division(10, 0))
# print(calculadora.division(10, ""))

# Ejercicio 2
# Crea una funcion que valide si un texto o palbra, es un palindromo
# utilizar operador de entrada y salida, condicionales y funciones

# Anita lava la tina
# Amo la pacifica paloma
# Oir a diario
# Oiras orar a Rosario
# Yo hago yoga hoy
# Ojo rojo

def palindromo(texto):
    cadena = texto.lower().replace(' ', '')
    return cadena == cadena[::-1] # [inicio:fin:pasos]

# palabra = input("Ingrese la palabra a validar: ")
# if palindromo(palabra):
#     print(f'{palabra} es un palindromo')
# else:
#     print(f'{palabra} no es un palindromo')

# Ejercicio 3
# Crear una funcion que se use como un juego, este juego sera para adivinar el numero aleatorio
# el usuario tendra que adivinar dicho numero
# utilizar bucles, condicionales y operadores para poder guiar al usuario en ingresar el numero correcto
# Si el numero ingresado es mayor al numero aleatorio mostrarle el mensaje al usuario
# Lo mismo si el numero ingresado es menor al numero aleatorio

from random import randint

def adivinar_numero():
    numero_secreto = randint(1, 100)
    intentos = 0

    while True:
        numero = int(input("Intenta adivinar el número (entre el 1 al 100): "))
        intentos += 1

        if numero < numero_secreto:
            print('El número es mayor, intenta nuevamente.')
        elif numero > numero_secreto:
            print('El número es menor, intenta nuevamente.')
        else:
            print(f"!Felicidades!, adivinates el numero en {intentos} intentos")
            break

# Ejercicio 4
# Crear una clase Estudiante, con los atributos nombre, edad, grado
# Luego usa una estructura de datos (lista), para almacenar los estudiantes
# Opc: la clase en mención debe tener los metodos de agregar y mostrar estudiantes

class Estudiante:
    def __init__(self, nombre, edad, grado):
        self.nombre = nombre
        self.edad = edad
        self.grado = grado

# lista_estudiantes = [
#     Estudiante("Stephan", 25, "3er Secundaria"),
#     Estudiante("Josue", 24, "2do Secundaria")
# ]
# 
# for estudiante in lista_estudiantes:
#     print(f'Nombre: {estudiante.nombre} | Edad: {estudiante.edad} | Grado: {estudiante.grado}')


# Ejercicio 5
# Crear una clase Empleado, con los atributos nombre, salario y area.
# Luego crea una lista y agrega los objetos de empleados nuevos, luego itera la lista mostrando a los empleados
# registrados

# Ejercicio 6
# Simular el juego de piedra papel y tijera
from random import choice

def juego_piedra_papel_tijeras(eleccion):
    opciones = ["piedra", "papel", "tijeras"]
    computadora = choice(opciones)

    print(f'La computadora escogio: {computadora}')

    # if eleccion == computadora:
    #     return 'Empate !'
    # elif (eleccion == "piedra" and computadora == "tijeras") or \
    #       (eleccion == "papel" and computadora == "piedra") or \
    #         (eleccion == "tijeras" and computadora == "papel"):
    #     return 'Ganaste !'
    # else:
    #     return 'Perdiste !'
    
    resultados = {
        'piedra': {
            'piedra': 'Empate',
            'tijeras': 'Ganamos',
            'papel': 'Perdemos'
        },
        'papel': {
            'papel': 'Empate',
            'tijeras': 'Perdemos',
            'piedra': 'Ganamos'
        },
        'tijeras': {
            'tijeras': 'Empate',
            'papel': 'Ganamos',
            'piedra': 'Perdemos'
        }
    }

    return resultados[eleccion][computadora]

print(juego_piedra_papel_tijeras("piedra"))
