![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# Entornos virtuales

## VENV

- Crear un entorno virtual

```sh
python -m venv "nombre_entorno"
```

- Activar el entorno virtual

```sh
source "nombre_entorno"/Scripts/activate
```

- Desactivar el entorno virtual

```sh
deactivate
```

### PIP

- Listar los paquetes instalados

```sh
pip list
```

- Instalar un paquete

```sh
pip install "nombre_paquete"
############################
pip install "nombre_paquete"==0.0.1
```

- Instalar paquetes desde requirements.txt

```sh
pip install -r requirements.txt
```

- Crear archivo de instalación de paquetes

```sh
pip freeze > requirements.txt
```

- Desinstalar un paquete

```sh
pip uninstall "nombre_paquete"
```

## PIPENV

- Crear entorno virtual

```sh
pipenv install
```

- Activar entorno virtual

```sh
pipenv shell
```

- Instalar un paquete

```sh
pipenv install "nombre_paquete"
###############################
pipenv install "nombre_paquete"==0.0.1
```

- Instalar un paquete de desarrollo

```sh
pipenv install "nombre_paquete" --dev
```

- Actualización de los paquetes

```sh
pipenv update
```

- Mostrar los paquetes y sus subpaquetes

```sh
pipenv graph
```

- Instalar las depedencias del proyecto

```sh
pipenv sync
```

- Desinstalar paquete

```sh
pipenv uninstall "nombre_paquete"
```

- Forzar la actualización del lock

```sh
pipenv lock
```

- Salir del entorno virtual

```sh
exit
```

## Formatos de texto (Python)

- Camelcase (No Python)

```
camelCase
```

- Snakecase, para declarar variables y funciones o metodos

```
snake_case
```

- Pascalcase, para declarar clases

```
PascalCase
```

- KebabCase (No Python)

```
kebab-case
```

# PARADIGMA DE LA PROGRAMACIÓN

1. Programación imperativa
2. Programación Orientada a Objetos (POO - OOP)
3. Programación funcional
4. Programación Declarativa
