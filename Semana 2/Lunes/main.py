# 1º Usando import
# import helpers

# 2º Usando from import
# from helpers import *

# 3º Usando from import
from app import ESPECIALIDAD
from app.helpers import sumar as suma

from camelcase import CamelCase

camel = CamelCase()
mensaje = "Hola a todos, bienvenidos a tecsup !"


print(ESPECIALIDAD)
print(suma(5, 4))
print(camel.hump(mensaje))
