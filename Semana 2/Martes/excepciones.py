# Manejo de excepciones
# try:
#     # Codigo que podria causar una excepcion
#     pass
# except:
#     # Que hacer si esa excepcion ocurre
#     pass

try:
    division = 10 / 5
except ZeroDivisionError:
    print('No se puede dividir por cero')
except TypeError:
    print('Uno de los valores ingresados es un texto, recuerda que deben ser solo números')
except Exception as e:
    # print(e.__class__)
    print('Ocurrio un error')
else:
    # Este bloque se ejecuta si no hay ninguna excepcion
    print(f"La división fue exitosa, el resultado es: {division}")
finally:
    # Este bloque se ejecuta asi hay o no hay una excepcion
    print("Se ejecuto")
