# PROGRAMACION ORIENTADA A OBJETOS (POO)
# Clase
class NombreClase: # PascalCase
    pass

# 1º Abstraccion, es el proceso de definir las caracteristicas de un objeto,
# separando la representacion detallada y subyacente.
class Ejemplo:
    # atributo de clase
    atributo_clase = "valor del atributo de clase"

    # atributo de instancia
    def __init__(self, valor):
        self.atributo_instancia = valor

class Auto:
    def __init__(self, marca, modelo): # this
        self.marca = marca
        self.model = modelo

    def encender(self):
        print("El auto esta encendido")

# Ejemplo.atributo_clase = "nuevo valor"

# instancia = Ejemplo("Valor 1") # objeto
# print(instancia.atributo_clase)
# print(instancia.atributo_instancia)

# instancia_dos = Ejemplo("Valor 2")
# print(instancia_dos.atributo_clase)
# print(instancia_dos.atributo_instancia)

# 2º Polimorfismo, la capacidad de tomar varias formas. Significa que una clase
# puede ser tratada como instancia de otra clase.
class Alumno:
    def __init__(self, nombre, aula):
        self.nombre = nombre
        self.aula = aula

    def presentacion(self):
        print(f'El alumno {self.nombre} se encuentra en el aula {self.aula}')


# gianmarco = Alumno('Gianmarco', 'backend')
# gianmarco.presentacion()

# mario = Alumno('Mario', 'frontend')
# mario.presentacion()

# 3º Encapsulacion, es el proceso de esconder detalles internos del objeto y exponerlo
# solo si es necesario.
class CuentaBancaria:
    def __init__(self, titular, balance=0):
        self.titular = titular
        self.__balance = balance

    def depositar(self, monto):
        self.__balance += monto
        return self.__balance
    
    def retirar(self, monto):
        if monto > self.__balance:
            return 'Fondos insuficientes'
        self.__balance -= monto
        return self.__balance
    
    def consultar_balance(self):
        return self.__balance

# cuenta_uno = CuentaBancaria("Wilson")
# cuenta_uno.depositar(10)
# cuenta_uno.depositar(50)
# cuenta_uno.depositar(5)
# # regla de seguridad
# print(cuenta_uno.consultar_balance())
# # fin de regla de seguridad
# print(cuenta_uno.retirar(60))

# 4º Herencia, permite que de una clase herede caracteristicas y comportamientos de otra
# clase (PADRE (SUPERCLASE) E HIJO (SUBCLASE))
class Animal:
    def __init__(self, especie):
        self.especie = especie

    def ejemplo(self):
        pass

    def presentar(self):
        print(f'Soy un {self.especie}')

class Perro(Animal):
    def __init__(self, nombre):
        super().__init__("perro")
        self.nombre = nombre

    def presentar(self):
        print(f'Hola, soy {self.nombre} y soy un {self.especie}')

    def ladrar(self):
        print('Guau Guau')

firulais = Perro('Firulais')
firulais.presentar()
firulais.ladrar()

# ABC, @abstractmethod
