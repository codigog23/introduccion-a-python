# Programacion imperativa
# Calcular la suma de los primeros 10 números
suma = 0
for i in range(1, 11):
    suma += i
print(suma)

# Programacion funcional
# Elevar al cuadra todos los valores de una lista de números
# [1, 2, 3, 4] -> [1, 4, 9, 16]

def al_cuadrado(n):
    return n ** 2

numeros = [1, 2, 3, 4]
nueva_lista = map(al_cuadrado, numeros)
print(list(nueva_lista))